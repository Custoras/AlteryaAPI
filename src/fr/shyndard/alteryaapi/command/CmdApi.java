package fr.shyndard.alteryaapi.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaapi.method.RankInformation;
import net.md_5.bungee.api.ChatColor;

public class CmdApi implements CommandExecutor {

	String prefix = ChatColor.DARK_GRAY + "[" + ChatColor.RED + "API" + ChatColor.DARK_GRAY + "] ";
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return true;
		}
		Player player = (Player)sender;
		if(DataAPI.getPlayer(player).getRank().getPower() == 1) {
			if(args.length > 0) {
				if(args[0].equalsIgnoreCase("load")) {
					if(args.length == 2) {
						if(args[1].equalsIgnoreCase("player")) {
							for(PlayerInformation pi : DataAPI.getPlayerList().values()) {
								pi.actualize();
							}
							sender.sendMessage(prefix + ChatColor.GREEN + "Actualisation joueur effectu�e.");
						} else if(args[1].equalsIgnoreCase("rank")) {
							for(RankInformation ri : DataAPI.getRankList().values()) {
								ri.actualize();
							}
							sender.sendMessage(prefix + ChatColor.GREEN + "Actualisation grade effectu�e.");
						} else {
							sender.sendMessage(prefix + ChatColor.GRAY + "/staff load <player|rank>");
						}
					} else {
						sender.sendMessage(prefix + ChatColor.GRAY + "/staff load <player|rank>");
					}
				}
			} else {
				sender.sendMessage(prefix + ChatColor.GRAY + "/staff load");
			}
		} else {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
		}
		return true;
	}

}
