package fr.shyndard.alteryaapi.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.Main;
import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;

public class CmdPay implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return true;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!pi.hasPermission("pay.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return true;
		}
		if(args.length != 2) {
			sender.sendMessage(Main.getMoneyPrefix() + "/payer <pseudo> <montant>");
		} else {
			Player target = Bukkit.getPlayer(args[0]);
			if(target == null) {
				sender.sendMessage(Main.getMoneyPrefix() + ChatColor.RED + "Joueur introuvable.");
				return true;
			}
			if(target == player) {
				sender.sendMessage(Main.getMoneyPrefix() + ChatColor.RED + "Vous ne pouvez pas �changer avec vous m�me.");
				return true;
			}
			Float money;
			try {
				money = Float.parseFloat(args[1]);
			} catch(Exception ex) {
				sender.sendMessage(Main.getMoneyPrefix() + ChatColor.RED + "Veuillez indiquer une valeur enti�re ou flottante.");
				return true;
			}
			if(money < 1) {
				sender.sendMessage(Main.getMoneyPrefix() + ChatColor.RED + "Minimum de 1" + Main.getMoneySymbol() + " pour �changer.");
				return true;
			}
			if(pi.getMoney(false) < money) {
				sender.sendMessage(Main.getMoneyPrefix() + ChatColor.RED + "Vous n'avez pas assez d'argent.");
				return true;
			}
			pi.removeMoney(money, false, false);
			DataAPI.getPlayer(target).addMoney(money, false, false);
			target.sendMessage(Main.getMoneyPrefix() + ChatColor.GREEN + "Vous recevez " + money + Main.getMoneySymbol() + " de " + player.getName() + ".");
			sender.sendMessage(Main.getMoneyPrefix() + ChatColor.GREEN + "Transaction de " + money + Main.getMoneySymbol() + " avec " + target.getName() + " effectu�e.");
		}
		return true;
	}
}
