package fr.shyndard.alteryaapi.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.Main;
import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;

public class CmdMoney implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return true;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!pi.hasPermission("money.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return true;
		}
		if(args.length > 0 && pi.hasPermission("money.admin")) {
			Player target = Bukkit.getPlayer(args[0]);
			PlayerInformation target_pi = DataAPI.getPlayer(target);
			if(target == null) {
				sender.sendMessage(Main.getMoneyPrefix() + ChatColor.RED + "Joueur introuvable.");
				return true;
			}
			else if(args.length == 3) {
				Float money;
				try {
					money = Float.parseFloat(args[2]);
				} catch(Exception ex) {
					sender.sendMessage(Main.getMoneyPrefix() + ChatColor.RED + "Veuillez indiquer une valeur enti�re ou flottante.");
					return true;
				}
				if(args[1].equalsIgnoreCase("add")) {
					target_pi.addMoney(money, true, false);
					sender.sendMessage(Main.getMoneyPrefix() + ChatColor.GREEN + target.getName() + " re�oit " + ChatColor.GOLD + money + Main.getMoneySymbol());
				} 
				else if(args[1].equalsIgnoreCase("remove")) {
					target_pi.removeMoney(money, true, false);
					sender.sendMessage(Main.getMoneyPrefix() + ChatColor.GREEN + target.getName() + " perd " + ChatColor.GOLD +  money + Main.getMoneySymbol());
				}
				else if(args[1].equalsIgnoreCase("set")) {
					target_pi.setMoney(money, true, false);
					sender.sendMessage(Main.getMoneyPrefix() + ChatColor.GREEN + target.getName() + " a d�sormais " + ChatColor.GOLD +  money + Main.getMoneySymbol());
				} else {
					sender.sendMessage(ChatColor.RED + "Argument inconnu.");
				}
			} else {
				sender.sendMessage(Main.getMoneyPrefix() + target.getName() + " a " + ChatColor.GOLD + target_pi.getMoney(false) + ChatColor.GRAY + "/" + ChatColor.GOLD + target_pi.getMaxMoney() + Main.getMoneySymbol());
			}
		} else {
			sender.sendMessage(Main.getMoneyPrefix() + "Vous avez " + ChatColor.GOLD + pi.getMoney(false) + ChatColor.GRAY + "/" + ChatColor.GOLD + pi.getMaxMoney() + Main.getMoneySymbol());
		}
		return true;
	}
}
