package fr.shyndard.alteryaapi.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import net.md_5.bungee.api.ChatColor;

public class CmdPuuid implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return true;
		}
		Player player = (Player)sender;
		if(args.length == 1 && DataAPI.getPlayer(player).isStaff()) {
			try {
				Player target = Bukkit.getPlayer(args[0]);
				sender.sendMessage(ChatColor.GRAY + "UUID " + ChatColor.WHITE + ChatColor.BOLD + target.getUniqueId());
			} catch(Exception ex) {
				sender.sendMessage(ChatColor.RED + "Joueur introuvable.");
			}
		}
		else sender.sendMessage(ChatColor.GRAY + "UUID " + ChatColor.WHITE + ChatColor.BOLD + player.getUniqueId());
		return true;
	}

}
