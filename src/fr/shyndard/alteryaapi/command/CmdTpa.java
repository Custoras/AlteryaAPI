package fr.shyndard.alteryaapi.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.function.TpFunction;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;

public class CmdTpa implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return true;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!pi.hasPermission("tpa.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return true;
		}
		if(args.length != 1) {
			sender.sendMessage(ChatColor.GRAY + "Utilisation /tpa <pseudo>");
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if(target == null) {
			sender.sendMessage(ChatColor.RED + "Ce joueur est introuvable.");
			return true;
		}
		TpFunction.sendRequest(player, target);
		return true;
	}
}
