package fr.shyndard.alteryaapi;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scheduler.BukkitRunnable;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.command.CmdApi;
import fr.shyndard.alteryaapi.command.CmdMoney;
import fr.shyndard.alteryaapi.command.CmdPay;
import fr.shyndard.alteryaapi.command.CmdPid;
import fr.shyndard.alteryaapi.command.CmdPing;
import fr.shyndard.alteryaapi.command.CmdPuuid;
import fr.shyndard.alteryaapi.event.PlayerChat;
import fr.shyndard.alteryaapi.event.PlayerConnection;
import fr.shyndard.alteryaapi.event.PlayerInteract;
import fr.shyndard.alteryaapi.function.LoadFunction;
import fr.shyndard.alteryaapi.function.NameColorManagement;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaapi.method.RankInformation;
import fr.shyndard.alteryaapi.method.Sql;
import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin implements PluginMessageListener {

	static Sql sql;
	static Main plugin;
	String proxy_channel = "alterya";
	static char money_symbol = '$';
	
	public void onEnable() {
		plugin = this;
		
		
		getCommand("api").setExecutor(new CmdApi());
		getCommand("money").setExecutor(new CmdMoney());
		getCommand("pay").setExecutor(new CmdPay());
		getCommand("pid").setExecutor(new CmdPid());
		getCommand("ping").setExecutor(new CmdPing());
		getCommand("puuid").setExecutor(new CmdPuuid());
		
		getServer().getPluginManager().registerEvents(new PlayerChat(), this);
		getServer().getPluginManager().registerEvents(new PlayerConnection(), this);
		getServer().getPluginManager().registerEvents(new PlayerInteract(), this);
		
		getServer().getMessenger().registerIncomingPluginChannel(this, proxy_channel, this);
	    getServer().getMessenger().registerOutgoingPluginChannel(this, proxy_channel);
		
		sql = new Sql("ThisIsAndOldParameter", 0, "ThisIsAndOldParameter", "ThisIsAndOldParameter", "ThisIsAndOldParameter");
		run();
		
		LoadFunction.loadRank();
		NameColorManagement.init();
	}
	
	public static Main getPlugin() {
		return plugin;
	}
	
	public static Sql getSql() {
		return sql;
	}
	
	public static char getMoneySymbol() {
		return money_symbol;
	}
	
	public static String getMoneyPrefix() {
		return ChatColor.WHITE + "[" + ChatColor.GOLD + ChatColor.BOLD + getMoneySymbol() + ChatColor.WHITE + "] " + ChatColor.GRAY;
	}
	
	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if (!channel.equals(proxy_channel)) {
            return;
        }
   
        ByteArrayInputStream stream = new ByteArrayInputStream(message);
        DataInputStream in = new DataInputStream(stream);
        try {
			final String[] args = in.readUTF().split("!");
			if(args[0].equals("actualizeplayer")) {
				if(args.length == 2) {
					if(Bukkit.getPlayer(args[1]) == null) return;
					DataAPI.getPlayerList().get(Bukkit.getPlayer(args[1])).actualize();
				} else {
					for(PlayerInformation pi : DataAPI.getPlayerList().values()) {
						pi.actualize();
						NameColorManagement.updateAll();
					}
				}
			}
			else if(args[0].equals("actualizerank")) {
				if(args.length == 2) {
					if(DataAPI.getRankList().get(Integer.parseInt(args[1])) == null) return;
					DataAPI.getRankList().get(Integer.parseInt(args[1])).actualize();
				} else {
					for(RankInformation ri : DataAPI.getRankList().values()) {
						ri.actualize();
						NameColorManagement.init();
						NameColorManagement.updateAll();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void run() {
		new BukkitRunnable() {
			@Override
	        public void run() {
				try {
					Statement state = sql.getConnection().createStatement();
					state.executeUpdate("UPDATE system_parameter SET value = '"+System.currentTimeMillis()/1000+"' WHERE parameter = 'last_actualization_minecraft'");
					state.close();
				} catch (SQLException e) {e.printStackTrace();}
	        }
		}.runTaskTimer(Main.getPlugin(), 0, 5*20);
	}
}
