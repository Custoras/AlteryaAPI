package fr.shyndard.alteryaapi.method;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import fr.shyndard.alteryaapi.Main;
import fr.shyndard.alteryaapi.api.DataAPI;

public class PlayerInformation {

	private Player player;
	private int player_id;
	private int rank_id;
	private boolean subscribe = false;
	private Map<String, Location> selection = new HashMap<>();
	private boolean show_tutorial = false;
	static String starChar = Character.toString('\u272A');
	static HashMap<UUID, PermissionAttachment> perms = new HashMap<UUID, PermissionAttachment>();
	
	public PlayerInformation(Player player) {
		this.player = player;
		DataAPI.addPlayer(player, this);
		actualize();
	}
	
	public void actualize() {
		perms.put(player.getUniqueId(), player.addAttachment(Main.getPlugin()));
		loadInfo();
		loadPermission();
		if(getRank().getPower() == 1 && !player.isOp()) player.setOp(true);
		//else if(player.isOp()) player.setOp(false);
		player.setPlayerListName((subscribe ? ChatColor.GOLD + starChar : "") + getRank().getColor() + (getRank().displayRankTab() ? "["+getRank().getName()+"] " : "") + player.getName());
		player.setDisplayName((subscribe ? ChatColor.GOLD + starChar : "") + getRank().getColor() + (getRank().displayRankTab() ? "["+getRank().getName()+"] " : "") + player.getName() + ChatColor.RESET);
	}
	
	private void loadInfo() {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT id, rank_id FROM player_information WHERE uuid = '"+player.getUniqueId().toString()+"'");
			while(result.next()) {
				player_id = result.getInt("id");
        		rank_id = result.getInt("rank_id");
        	}
			result = state.executeQuery("SELECT 1 FROM web.player_subscription WHERE player_id = "+player_id+" AND subscription_time_end > " + (System.currentTimeMillis()/1000));
			while(result.next()) {
				subscribe = true;
        	}
			return;
		} catch (SQLException e) {e.printStackTrace();}
		rank_id = 15;
	}
	
	private void loadPermission() {
		PermissionAttachment pperms = perms.get(player.getUniqueId());
		for(String val : pperms.getPermissions().keySet()) perms.get(player.getUniqueId()).unsetPermission(val);
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery(""
					+ "SELECT permission FROM player_permission WHERE player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') "
					+ "UNION " 
			        + "SELECT permission FROM rank_permission WHERE rank_power >= (SELECT power FROM rank_information AS RI JOIN player_information AS PI ON PI.rank_id = RI.id WHERE PI.uuid = '"+player.getUniqueId()+"') "
			        );
			while(result.next()) pperms.setPermission(result.getString("permission"), true);
		} catch (SQLException e) {e.printStackTrace();}
		
	}
	
	private boolean hasSQLPermission(String perm) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM "
				+ "( "
					+ "SELECT permission FROM player_permission WHERE player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') "
					+ "UNION " 
			        + "SELECT permission FROM rank_permission WHERE rank_power >= (SELECT power FROM rank_information AS RI JOIN player_information AS PI ON PI.rank_id = RI.id WHERE PI.uuid = '"+player.getUniqueId()+"') "
			    + " ) AS permission_list "
			+ "WHERE permission = '"+perm+"'");
			while(result.next()) return true;
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	
	public int getId() { return player_id; }
	
	public boolean isStaff() { return getRank().getPower() <= 5; }
	
	public boolean isShowingTutorial() { return show_tutorial; }
	
	public boolean isSubscribe() { return subscribe; }
	
	public void setShowingTutorial(boolean value) { show_tutorial = value; }
	
	public Map<String, Location> getSelection() {
		return selection;
	}
	
	public RankInformation getRank() {
		try {
			return DataAPI.getRankList().get(rank_id);
		} catch(Exception ex) { ex.printStackTrace(); }
		return DataAPI.getRankList().get(15);
	}
	
	public boolean hasPermission(String permission) {
		if(getRank().getPower() <= 1) return true;
		return hasSQLPermission(permission);
	}
	
	public void setRank(Integer rank_id, boolean message) {
		RankInformation ri = DataAPI.getRankList().get(rank_id);
		if(ri == null) return;
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE player_information SET rank_id = " + rank_id + " WHERE uuid = '" + player.getUniqueId() + "'");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
		actualize();
		if(message) {
			player.sendMessage(ChatColor.GRAY + "Vous obtenez le rang " + ri.getColor() + ri.getName());
			player.sendMessage(ChatColor.GRAY + "En cas de probl�me, faites un d�co/reco.");
		}
	}
	
	public float getMaxMoney() {
		if(getRank().getPower() == 1) return Integer.MAX_VALUE;
		if(getRank().getPower() <= 7) return 6500F;
		if(getRank().getPower() == 8) return 2500F;
		return 1000F;
	}
	
	public void setMoney(Float value, boolean message, boolean bank) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE player_information SET " + (bank ? "bank" : "money") + " = " + value + " WHERE uuid = '" + player.getUniqueId() + "'");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
		if(message) player.sendMessage(Main.getMoneyPrefix() + "Vous avez " + ChatColor.GOLD + value + Main.getMoneySymbol() + (bank ? ChatColor.GRAY + " en banque." : ""));
	}
	
	public void addMoney(Float value, boolean message, boolean bank) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE player_information SET " + (bank ? "bank" : "money") + " = (" + (bank ? "bank" : "money") + " + " + value + ") WHERE uuid = '" + player.getUniqueId() + "'");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
		if(message) player.sendMessage(Main.getMoneyPrefix() + "Vous recevez " + ChatColor.GOLD + value + Main.getMoneySymbol() + (bank ? ChatColor.GRAY + " en banque." : ""));
	}
	
	public void removeMoney(Float value, boolean message, boolean bank) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE player_information SET " + (bank ? "bank" : "money") + " = (" + (bank ? "bank" : "money") + " - " + value + ") WHERE uuid = '" + player.getUniqueId() + "'");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
		if(message) player.sendMessage(Main.getMoneyPrefix() + "Vous perdez " + ChatColor.GOLD + value + Main.getMoneySymbol() + (bank ? ChatColor.GRAY + " en banque." : ""));
	}
	
	public float getMoney(boolean bank) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT " + (bank ? "bank" : "money") + " AS money FROM player_information WHERE uuid = '" + player.getUniqueId() + "'");
			while(result.next()) {
        		return result.getFloat("money");
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return 0;
	}
}
