package fr.shyndard.alteryaapi.method;

import org.bukkit.entity.Player;

public class TeleportationRequest {

	private Player sender;
	private Player target;
	private long timestamp;
	
	public TeleportationRequest(Player sender, Player target) {
		this.sender = sender;
		this.target = target;
		this.timestamp = System.currentTimeMillis()/1000;
	}
	
	public Player getSender() {
		return sender;
	}
	
	public Player getTarget() {
		return target;
	}
	
	public boolean isValable() {
		return (timestamp + 60 >= System.currentTimeMillis()/1000);
	}
}
