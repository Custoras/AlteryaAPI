package fr.shyndard.alteryaapi.method;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Title {

	String title;
	String subTitle;
	Integer fadeIn;
	Integer stay;
	Integer fadeOut;
	
	public Title(String title, String subTitle, Integer fadeIn, Integer stay, Integer fadeOut) {
		this.title = title;
		this.subTitle = subTitle;
		this.fadeIn = fadeIn;
		this.stay = stay;
		this.fadeOut = fadeOut;
		if(fadeIn < 5) {
			fadeIn = 5;
		}
		if(fadeOut < 5) {
			fadeOut = 5;
		}
	}
	
	public void send(Player player) {
		APITitle.sendTitle(player,fadeIn,stay,fadeOut,title,subTitle);
	}
	
	public void broadcast() {
		for(Player players : Bukkit.getOnlinePlayers()) {
			send(players);
		}
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	
}
