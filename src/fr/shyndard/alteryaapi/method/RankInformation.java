package fr.shyndard.alteryaapi.method;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.bukkit.ChatColor;

import fr.shyndard.alteryaapi.Main;
import fr.shyndard.alteryaapi.api.DataAPI;

public class RankInformation {

	private int id;
	private int power;
	private String name;
	private String chat_format;
	private ChatColor color;
	private boolean display_rank_tab;
	
	public RankInformation(int id) {
		this.id = id;
		actualize();
		DataAPI.addRank(id, this);
	}
	
	public void actualize() {
		loadInformation();
	}
	
	private void loadInformation() {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT power, name, chat_format, color, display_rank_tab FROM rank_information WHERE id = " + id);
			while(result.next()) {
				power = result.getInt("power");
				name = result.getString("name");
				color = ChatColor.getByChar(result.getString("color"));
				chat_format = result.getString("chat_format").replaceAll("%name%", name);
				chat_format = ChatColor.translateAlternateColorCodes('&', chat_format);
				display_rank_tab = result.getBoolean("display_rank_tab");
        	}
		 } catch (SQLException e) {e.printStackTrace();}
	}
	
	public int getId() {
		return id;
	}
	
	public int getPower() {
		return power;
	}
	
	public String getName() {
		return name;
	}
	
	public String getChatFormat() {
		return chat_format;
	}
	
	public ChatColor getColor() {
		return color;
	}
	
	public boolean displayRankTab() {
		return display_rank_tab;
	}
}
