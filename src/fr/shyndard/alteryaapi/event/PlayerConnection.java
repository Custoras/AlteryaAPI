package fr.shyndard.alteryaapi.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;

public class PlayerConnection implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void playerLoginLow(PlayerLoginEvent event) {
		new PlayerInformation(event.getPlayer());
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		event.setJoinMessage(null);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onLeave(PlayerQuitEvent event) {
		event.setQuitMessage(null);
		DataAPI.getPlayerList().remove(event.getPlayer());
	}
}
