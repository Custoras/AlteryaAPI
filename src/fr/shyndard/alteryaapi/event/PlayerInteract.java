package fr.shyndard.alteryaapi.event;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;

public class PlayerInteract implements Listener {

	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		PlayerInformation pi = DataAPI.getPlayer(event.getPlayer());
		if(event.getClickedBlock() != null && event.getPlayer().getInventory().getItemInMainHand().getType() == Material.STICK && event.getPlayer().isOp()) {
			if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
				pi.getSelection().put("p1", event.getClickedBlock().getLocation());
				event.setCancelled(true);
				event.getPlayer().sendMessage(ChatColor.GRAY + "Position 1 enregistrée.");
			}
			else if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				pi.getSelection().put("p2", event.getClickedBlock().getLocation());
				event.setCancelled(true);
				event.getPlayer().sendMessage(ChatColor.GRAY + "Position 2 enregistrée.");
			}
		}
	}
}
