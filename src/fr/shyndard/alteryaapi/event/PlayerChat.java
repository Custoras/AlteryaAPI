package fr.shyndard.alteryaapi.event;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import fr.shyndard.alteryaapi.api.DataAPI;

public class PlayerChat implements Listener {
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		if(!event.isCancelled()) {
			if(DataAPI.getPlayer(event.getPlayer()).isStaff()) {
				event.setMessage(ChatColor.translateAlternateColorCodes('&', event.getMessage()));
			}
		}
	}
}
