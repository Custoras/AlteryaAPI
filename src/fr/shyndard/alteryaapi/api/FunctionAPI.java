package fr.shyndard.alteryaapi.api;

import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class FunctionAPI {

	public static void question(Player player, String message, String cmd) {
		ClickEvent clickEvent =  new ClickEvent(ClickEvent.Action.RUN_COMMAND, cmd);
		BaseComponent[] message_to_send = new ComponentBuilder(message).color(ChatColor.GRAY).event(clickEvent).create();
		player.spigot().sendMessage(message_to_send);
	}
}
