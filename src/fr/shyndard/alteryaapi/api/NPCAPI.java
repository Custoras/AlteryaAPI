package fr.shyndard.alteryaapi.api;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class NPCAPI {

	public static void talkTo(NPC npc, Player player, String text) {
		player.sendMessage(ChatColor.DARK_PURPLE + npc.getFullName() + ChatColor.GRAY + "> " + text);
	}
	
	public static void talkTo(NPC npc, Player player, String text, Integer quest_id) {
		talkTo(npc, player, text);
		answer(player, "J'accepte !", "/quest talknpc " + npc.getId() + " accept " + quest_id);
	}
	
	public static void talkRadius(NPC npc, String text, Integer radius) {
		for(Entity e : npc.getStoredLocation().getWorld().getNearbyEntities(npc.getStoredLocation(), radius, 4, radius)) {
			if(e.getType() == EntityType.PLAYER) talkTo(npc, (Player)e, text);
		}
	}
	
	public static void answer(Player player, String message, String cmd) {
		ClickEvent clickEvent =  new ClickEvent(ClickEvent.Action.RUN_COMMAND, cmd);
		BaseComponent[] message_to_send = new ComponentBuilder("- " + message).color(ChatColor.GREEN).event(clickEvent).create();
		player.spigot().sendMessage(message_to_send);
	}
}
