package fr.shyndard.alteryaapi.api;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.Main;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaapi.method.RankInformation;
import fr.shyndard.alteryaapi.method.Sql;

public class DataAPI {

	static Map<Player, PlayerInformation> player_list = new HashMap<>();
	static Map<Integer, RankInformation> rank_list = new HashMap<>();
	
	public static String getLine(ChatColor color) {
		return ("" + color + ChatColor.STRIKETHROUGH + StringUtils.repeat(" ", 80));
	}
	
	public static char getMoneySymbol() {
		return Main.getMoneySymbol();
	}
	
	public static Map<Player, PlayerInformation> getPlayerList() {
		return player_list;
	}
	
	public static Map<Integer, RankInformation> getRankList() {
		return rank_list;
	}
	
	public static PlayerInformation getPlayer(Player player) {
		return player_list.get(player);
	}
	
	public static void addPlayer(Player player, PlayerInformation value) {
		player_list.put(player, value);
	}
	
	public static void addRank(Integer id, RankInformation value) {
		rank_list.put(id, value);
	}
	
	public static Sql getSql() {
		return Main.getSql();
	}
	
	public static String getPlayerName(UUID uuid) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT name FROM player_information WHERE uuid = '" + uuid + "'");
			while(result.next()) {
				return result.getString("name");
			}
		} catch (SQLException e) {e.printStackTrace();}
		return null;
	}
	
}
