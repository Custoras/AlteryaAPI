package fr.shyndard.alteryaapi.function;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.shyndard.alteryaapi.Main;
import fr.shyndard.alteryaapi.method.RankInformation;

public class LoadFunction {

	public static void loadRank() {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT id FROM rank_information");
			while(result.next()) {
				new RankInformation(result.getInt("id"));
        	}
		} catch (SQLException e) {e.printStackTrace();}
	}
}
