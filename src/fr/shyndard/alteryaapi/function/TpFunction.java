package fr.shyndard.alteryaapi.function;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.FunctionAPI;
import fr.shyndard.alteryaapi.method.TeleportationRequest;
import net.md_5.bungee.api.ChatColor;

public class TpFunction {

	static List<TeleportationRequest> tr_list = new ArrayList<>();
	
	public static void sendRequest(Player player, Player target) {
		target.sendMessage(ChatColor.YELLOW + player.getName() + ChatColor.GRAY + " veut se t�l�porter � vous.");
		FunctionAPI.question(target, ChatColor.GREEN + "Accepter sa demande de t�l�portation", "/tpaccept " + player.getName());
		player.sendMessage(ChatColor.GRAY + "Demande de t�l�portation envoy�e � " + target.getName());
		tr_list.add(new TeleportationRequest(player, target));
	}
	
	public static boolean isRequestValable(Player player, Player target) {
		for(TeleportationRequest tr : tr_list) {
			if(tr.getSender() == player && tr.getTarget() == target) {
				if(tr.isValable()) return true;
				tr_list.remove(tr);
			}
		}
		return false;
	}
}
