package fr.shyndard.alteryaapi.function;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.RankInformation;

@SuppressWarnings("deprecation")
public class NameColorManagement {

	static Map<Integer, Team> team_list = new HashMap<>();
	static ScoreboardManager manager;
	static Scoreboard board;
	
	public static void init() {
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
		for(RankInformation ri : DataAPI.getRankList().values()) {
			Team team = board.registerNewTeam(ri.getName());
			team.setColor(ri.getColor());
			team.setPrefix(ri.getColor() + "");
		}
	}
	
	public static void updateAll() {
		for(Player p : Bukkit.getOnlinePlayers()) update(p);
	}
	public static void update(Player player) {
		remove(player);
		board.getTeam(DataAPI.getPlayer(player).getRank().getName()).addPlayer(player);
		player.setScoreboard(board);
	}

	public static void remove(Player player) {
		for(Team t : board.getTeams()) t.removePlayer(player);
	}
}
