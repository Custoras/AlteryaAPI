package fr.shyndard.alteryaapi.function;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.Main;

public class SqlFunction {

	public static boolean isOwnerRestrictAccessEnable() {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT value FROM system_parameter WHERE parameter = 'owner_restrict_access';");
			while(result.next()) {
        		return result.getBoolean("value");
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return true;
	}

	public static Integer getPlayerId(Player sender) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT id FROM player_information WHERE uuid = '"+sender.getUniqueId()+"';");
			while(result.next()) return result.getInt("id");
		} catch (SQLException e) {e.printStackTrace();}
		return 0;
	}
}
